---
layout: post
title:  "Bem-vindo, Jekyll!"
date:   2015-01-22 17:39:42
categories: jekyll bem-vindo install
---


Pois é, aderi ao Jekyll. 


Poderia muito bem fazer uso do Wordpress, mas há algumas coisas que não gosto nele, outras que simplesmente não utilizo e já tive problemas sérios com plugins de terceiros 'mal-feitos' que expôem o servidor inteiro a vulnerabilidades.

Somado-se ao fato de que o Jekyll utiliza a 'linguagem de marcação' favorita de todos os documentadores desse nosso mundão: o *Markdown*. Com isso, **meu foco é voltado mais para o texto e menos para a questão visual** (tags no post, media e etc). Basta apenas escrever o texto que a formatação acontece de forma automática ao renderizar a página. Ou seja, você consegue criar um post usando apenas o Bloco de Notas, TextPad, Notepad++, SciTE, Vim, Emacs, mcedit (o edit do Midnight Commander)... Claro que, se você precisa alterar algum elemento do layout, vai ter que botar a mão na massa e alterar alguma coisa em HTML. Mas também não é nada de outro mundo.



Há quem diga que o Jekyll é ruim pois é um sistema de páginas estáticas. É o preço que pagamos por optar por um CMS com uma melhor performance que o WP (e sinceramente), preço baixíssimo. E também não preciso instalar um WP Super Cache da vida para evitar aquela enxurrada de conexões simultâneas ao BD e tomar gancho do hosting.

Posteriormente vamos falar um pouco mais sobre ele - quem sabe até um tutorial básico de como instalá-lo em seu VPS. Mas se você quiser tentar, fique à vontade, [acessando a página oficial do Jekyll](http://jekyllrb.com/).

Só espero que ele não vire um mr. Hyde =)


[Não acredito que você não conhece o Markdown](http://daringfireball.net/projects/markdown/syntax)!